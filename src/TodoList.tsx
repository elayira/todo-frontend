import { useReducer, useRef, useEffect } from 'react';
import styles from './TodoList.module.css'

interface Task {
    id: string;
    name: string;
    completed: boolean;
    closedForUpdate?: boolean;
}

enum TaskActions {
    ADD_TASK,
    DELETE_TASK,
    UPDATE_TASK,
    COMPLETED,
}

interface ITodoList {
    type: TaskActions;
    payload: Partial<Task>;
}

function TodoList() {
    const taskElement = useRef<HTMLInputElement>(null)
    const [state, dispatch] = useReducer(todoFormReducer, []);

    useEffect(() => {
        if ( taskElement.current ) taskElement.current.focus();
    }, []);

    function todoFormReducer(state: Task[], dispatch: ITodoList) {
        const { type, payload } = dispatch
        
        switch (type) {
            case TaskActions.ADD_TASK:
                if (taskElement.current) {
                    taskElement.current.value = ''
                }
                payload.closedForUpdate = true
                return [...state, payload as Task]
            case TaskActions.DELETE_TASK:
                return state.filter(task => task.id !== payload.id)
            case TaskActions.COMPLETED:
                return state.map(task => task.id === payload.id? {...task, completed: !task.completed} : task)
            case TaskActions.UPDATE_TASK:
                return state.map(task => task.id === payload.id? {...task, ...payload} : task)
            default:
                return state;
        }
    }

    return (
        <>
            <h1 className={styles.title}>Todo List</h1>
            <div className={styles.container}>
                <input 
                    type="text"
                    ref={taskElement}
                    placeholder='Task Name'
                    onKeyUp={
                        ((e) => e.key === 'Enter'? 
                            dispatch({
                                type: TaskActions.ADD_TASK, 
                                payload: { 
                                    id: `${Math.random()}`, 
                                    name: taskElement.current?.value as string, 
                                    completed: false 
                                }
                            }): 
                            e.preventDefault()
                        )
                    }  
                />
                <button 
                    onClick={
                        () => dispatch({
                            type: TaskActions.ADD_TASK, 
                            payload: { id: `${Math.random()}`, 
                            name: taskElement.current?.value as string, 
                            completed: false 
                        }})
                    }
                >  
                    Add Task
                </button>
            </div>
            <hr />
            <div className={styles.container}>
                <ul>
                    {state.map((task) => (
                        <li key={task.id}>
                            <input 
                                type="checkbox"
                                checked={task.completed}
                                name='task-status'
                                onChange={
                                    () => dispatch({
                                        type: TaskActions.COMPLETED, 
                                        payload: {id: task.id}
                                    })
                                } 
                            />
                            
                            <input 
                                type="text"
                                value={task.name}
                                name='update-task-name'
                                onChange={
                                    (e) => dispatch({
                                        type: TaskActions.UPDATE_TASK, 
                                        payload: {id: task.id, name: e.target.value}
                                    })
                                }
                                disabled={task.closedForUpdate}
                            />
                            
                            <button
                                name='edit-task'
                                onClick={
                                    () => dispatch({
                                        type: TaskActions.UPDATE_TASK, 
                                        payload: {id: task.id, closedForUpdate: !task.closedForUpdate}
                                    })
                                }
                            >
                                { task.closedForUpdate? `Edit`: `Done` }
                            </button>
                            
                            <button
                                name='delete-task'
                                onClick={
                                    () => dispatch({
                                        type: TaskActions.DELETE_TASK, 
                                        payload: {id: task.id}
                                    })
                                }
                            >
                                Delete
                            </button>
                        </li>
                    ))}
                </ul>
            </div>
        </>
    );
}

export default TodoList;
